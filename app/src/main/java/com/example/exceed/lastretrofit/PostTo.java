package com.example.exceed.lastretrofit;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by exceed on 4/6/16 AD.
 */
public interface PostTo {
//    @FormUrlEncoded
    @POST("/RetrofitTest/book.json")
    Call<Book> createBook(@Body Book book);
//    Call<Book> postScans(
//            @Field("bookId") int a,
//            @Field("name") String b,
//            @Field("price") String c,
//            @Field("inStock") int d
//    );
//    void sendBook(@Body Book book,Callback<List<Book>> r);

//    Call<Book> createBook(@Body Book book);
}
