package com.example.exceed.lastretrofit;

import com.google.gson.annotations.SerializedName;

public class Book {

    //Variables that are in our json

    @SerializedName("bookId")
    private int bookId;

    @SerializedName("name")
    private String name;

    @SerializedName("price")
    private String price;

    @SerializedName("inStock")
    private int inStock;

    public Book(int bookId, String name, String price, int inStock) {
        this.bookId = bookId;
        this.name = name;
        this.price = price;
        this.inStock = inStock;
    }

    //Getters and setters
    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getInStock() {
        return inStock;
    }

    public void setInStock(int inStock) {
        this.inStock = inStock;
    }
}