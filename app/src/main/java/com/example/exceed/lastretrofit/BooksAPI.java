package com.example.exceed.lastretrofit;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Belal on 11/3/2015.
 */
public interface BooksAPI {

    /*Retrofit get annotation with our URL
       And our method that will return us the list ob Book
    */
    @GET("/RetrofitTest/book.json")
    Call<List<Book>> getBook();
//    void getBooks(Callback<List<Book>> response);


}