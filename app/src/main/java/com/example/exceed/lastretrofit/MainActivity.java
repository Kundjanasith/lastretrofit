package com.example.exceed.lastretrofit;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//import retrofit.RestAdapter;
//import retrofit.RetrofitError;
//import retrofit.client.Response;

//import retrofit.Callback;

public class MainActivity extends AppCompatActivity implements ListView.OnItemClickListener {

    //Root URL of our web service
//    public static final String ROOT_URL = "http://simplifiedcoding.16mb.com/";
    public static final String ROOT_URL = "http://www.kundjanasith.com";
    //Strings to bind with intent will be used to send data to other activity
    public static final String KEY_BOOK_ID = "key_book_id";
    public static final String KEY_BOOK_NAME = "key_book_name";
    public static final String KEY_BOOK_PRICE = "key_book_price";
    public static final String KEY_BOOK_STOCK = "key_book_stock";

    //List view to show data
    private ListView listView;

    //List of type books this list will store type Book which is our data model
    private List<Book> books;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        books = new ArrayList<>();
        //Initializing the listview
        listView = (ListView) findViewById(R.id.listViewBooks);

        //Calling the method that will fetch data
        getBooks();

        //Setting onItemClickListener to listview
//        listView.setOnItemClickListener(this);
        listView.setOnItemClickListener(this);
    }

    private void getBooks(){
        //While the app fetched data we are displaying a progress dialog
        final ProgressDialog loading = ProgressDialog.show(this,"Fetching Data","Please wait...",false,false);

        //Creating a rest adapter
//        RestAdapter adapter = new RestAdapter.Builder()
//                .setEndpoint(ROOT_URL)
//                .build();
//        Retrofit retrofit = new Retrofit.Builder().baseUrl(ROOT_URL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
//        Retrofit retrofit = new Retrofit.Builder().baseUrl(ROOT_URL).addConverterFactory(GsonConverterFactory.create()).build();

        //Creating an object of our api interface
        BooksAPI api = retrofit.create(BooksAPI.class);

        Call<List<Book>> call = api.getBook();
        call.enqueue(new Callback<List<Book>>() {
                @Override
                public void onResponse(Call<List<Book>> call, Response<List<Book>> response) {
//                    Response<List<Book>> rb = call.execute();
                    loading.dismiss();
                    books = response.body();
                    showList();
                }

                @Override
                public void onFailure(Call<List<Book>> call, Throwable t) {

                }
            });

//            books = call.execute().body();


//        Call<List<Book>> repo = api.getBook();
        //Defining the method
//        try {
//            books = api.getBook().execute().body();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        try {
//            books = repo.execute().body();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        api.getBooks(new Callback<List<Book>>() {
//            @Override
//            public void onResponse(Call<List<Book>> call, Response<List<Book>> response) {
//                //Dismissing the loading progressbar
//                loading.dismiss();
//
//                //Storing the data in our list
//                books = response.body();
//
//                //Calling a method to show the list
//                showList();
//            }
//
//            @Overrides
//            public void onFailure(Call<List<Book>> call, Throwable t) {
//
//            }
//
//        });
    }

    //Our method to show list
    private void showList(){
        //String array to store all the book names
        String[] items = new String[books.size()];

        //Traversing through the whole list to get all the names
        for(int i=0; i<books.size(); i++){
            //Storing names to string array
            items[i] = books.get(i).getName();
        }

        //Creating an array adapter for list view
        ArrayAdapter adapter = new ArrayAdapter<String>(this,R.layout.simple_list,items);

        //Setting adapter to listview
        listView.setAdapter(adapter);
    }


    //This method will execute on listitem click
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //Creating an intent
        Intent intent = new Intent(MainActivity.this, ShowBookDetails.class);

        //Getting the requested book from the list
        Book book = books.get(position);

        //Adding book details to intent
        intent.putExtra(KEY_BOOK_ID,book.getBookId());
        intent.putExtra(KEY_BOOK_NAME,book.getName());
        intent.putExtra(KEY_BOOK_PRICE,book.getPrice());
        intent.putExtra(KEY_BOOK_STOCK,book.getInStock());

        //Starting another activity to show book details
        startActivity(intent);
    }

    public void click(View v) throws JSONException, IOException {
        Book user = new Book(123, "John Doe","p",0);
        Log.i("click", user.getName());
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        PostTo service = retrofit.create(PostTo.class);
//        books.add(user);
        Call<Book> call = service.createBook(user);
        call.enqueue(new Callback<Book>() {
            @Override
            public void onResponse(Call<Book> call, Response<Book> response) {
                Log.i("xdxd",response.body()+"");
            }

            @Override
            public void onFailure(Call<Book> call, Throwable t) {
                Log.i("x0xd",t.getMessage());
            }
        });
//        Call<Book> call = service.createBook(user);
//        Response<Book> response = call.execute();

    }

}